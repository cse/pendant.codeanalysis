﻿## Release 1.0.0

### New Rules

Rule ID | Category | Severity | Notes
--------|----------|----------|--------------------
FAL0001 |  Design  |  Error   | FallacyAnalyzer
NAM0001 |  Naming  | Warning  | NamingConventionAnalyzer
NAM0002 |  Naming  | Warning  | NamingConventionAnalyzer
NAM0003 |  Naming  | Warning  | NamingConventionAnalyzer
NAM0004 |  Naming  | Warning  | NamingConventionAnalyzer
NAM0005 |  Naming  | Warning  | NamingConventionAnalyzer
NAM0006 |  Naming  | Warning  | NamingConventionAnalyzer
NAM0007 |  Naming  | Warning  | NamingConventionAnalyzer
NAM0008 |  Naming  | Warning  | NamingConventionAnalyzer
NAM0009 |  Naming  | Warning  | NamingConventionAnalyzer
NAM0010 |  Naming  | Warning  | NamingConventionAnalyzer
NAM0011 |  Naming  | Warning  | NamingConventionAnalyzer
NAM0012 |  Naming  | Warning  | NamingConventionAnalyzer
NAM0013 |  Naming  | Warning  | NamingConventionAnalyzer
NAM0014 |  Naming  | Warning  | NamingConventionAnalyzer
NAM0015 |  Naming  | Warning  | NamingConventionAnalyzer
NAM0016 |  Naming  | Warning  | NamingConventionAnalyzer