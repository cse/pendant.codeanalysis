﻿/* MisconceptionsAnalyzer.cs
 * Author: Nathan Bean
 */
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;
using System.Collections.Immutable;
using System.Linq;

namespace KSU.CS.Pendant.CodeAnalysis
{
    /// <summary>
    /// A Code analyzer that checks for properties that reference themselves, i.e.:
    /// <code>
    /// public string Foo {
    ///   get { return Foo; }
    ///   set { Foo = value; }
    /// }
    /// </code>
    /// This kind of self-reference creates and infinite recursion when the set or get method is invoked at runtime, 
    /// causing a StackOverflow
    /// </summary>
    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public class FallacyAnalyzer : DiagnosticAnalyzer
    {
        #region Metadata 

        // Property Self-Reference metadata
        public const string PSRDiagnosticId = "FAL0001";
        private static readonly LocalizableString PSRTitle = new LocalizableResourceString(nameof(Resources.PropertySelfReferenceAnalyzerTitle), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString PSRMessageFormat = new LocalizableResourceString(nameof(Resources.PropertySelfReferenceAnalyzerMessageFormat), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString PSRDescription = new LocalizableResourceString(nameof(Resources.PropertySelfReferenceAnalyzerDescription), Resources.ResourceManager, typeof(Resources));
        private const string PSRCategory = "Design";
        private static readonly DiagnosticDescriptor PropertySelfReferenceRule = new DiagnosticDescriptor(PSRDiagnosticId, PSRTitle, PSRMessageFormat, PSRCategory, DiagnosticSeverity.Error, isEnabledByDefault: true, description: PSRDescription);



        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics { get { return ImmutableArray.Create(PropertySelfReferenceRule); } }

        #endregion

        /// <summary>
        /// Initializes the PropertySelfReferenceAnalyzer
        /// </summary>
        /// <param name="context">The analysis context</param>
        public override void Initialize(AnalysisContext context)
        {
            context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.None);
            context.EnableConcurrentExecution();
            context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.PropertyDeclaration);
        }

        /// <summary>
        /// Analyzes a PropertyDeclarationSyntax Node for self-reference errors
        /// </summary>
        /// <param name="context">The context of the PropertyDeclarationSyntax Node</param>
        private static void AnalyzeNode(SyntaxNodeAnalysisContext context)
        {
            // The context Node should be a PropertyDeclarationSyntax
            var propertyDeclaration = (PropertyDeclarationSyntax)context.Node;

            // Grab and evaluate any accessor declarations that go with this property
            var accessorDeclarations = propertyDeclaration.DescendantNodes().OfType<AccessorDeclarationSyntax>();
            foreach (var dec in accessorDeclarations)
            {
                // Check for a get self-reference
                if (dec.IsKind(SyntaxKind.GetAccessorDeclaration))
                {
                    // Get any self-references
                    var getSelfReference = dec.DescendantNodes()
                        .OfType<IdentifierNameSyntax>()
                        .Where(id => id.Identifier.ValueText == propertyDeclaration.Identifier.ValueText && !(
                                // Ignore enumeration access using the same identifier as the Enum type
                                id.Parent.IsKind(SyntaxKind.SimpleMemberAccessExpression) ||
                                // Ignore variable declaration using the same identifier as the Type
                                id.Parent.IsKind(SyntaxKind.VariableDeclaration) &&
                                ((VariableDeclarationSyntax)id.Parent).Type.ToString() == id.Identifier.ValueText
                            ));
                    if (!getSelfReference.Any()) break;

                    // Create the diagnostic
                    var primaryLocation = propertyDeclaration.Identifier.GetLocation();
                    var additionalLocations = from sr in getSelfReference select sr.Identifier.GetLocation();
                    var diagnostic = Diagnostic.Create(
                        PropertySelfReferenceRule,
                        primaryLocation,
                        additionalLocations,
                        new string[] {propertyDeclaration.Identifier.ValueText, "get"}
                        );
                    context.ReportDiagnostic(diagnostic);
                }

                // Check for a set self-reference
                if (dec.IsKind(SyntaxKind.SetAccessorDeclaration))
                {
                    // Get any self-references
                    var setSelfReference = dec.DescendantNodes()
                        .OfType<IdentifierNameSyntax>()
                        .Where(id => id.Identifier.ValueText == propertyDeclaration.Identifier.ValueText && !(
                                // Ignore enumeration access using the same identifier as the Enum type
                                id.Parent.IsKind(SyntaxKind.SimpleMemberAccessExpression) ||
                                // Ignore variable declaration using the same identifier as the Type
                                id.Parent.IsKind(SyntaxKind.VariableDeclaration) &&
                                ((VariableDeclarationSyntax)id.Parent).Type.ToString() == id.Identifier.ValueText
                            ));
                    if (!setSelfReference.Any()) break;

                    // Create the diagnostic
                    var primaryLocation = propertyDeclaration.Identifier.GetLocation();
                    var additionalLocations = from sr in setSelfReference select sr.Identifier.GetLocation();
                    var diagnostic = Diagnostic.Create(
                        PropertySelfReferenceRule,
                        primaryLocation,
                        additionalLocations,
                        new string[] { propertyDeclaration.Identifier.ValueText, "set" }
                        );
                    context.ReportDiagnostic(diagnostic);

                }
            }
            // If there are *no* accessors, we might have the lambda shorthand to check
            if(!accessorDeclarations.Any())
            {
                var lambdaDec = propertyDeclaration.DescendantNodes().OfType<ArrowExpressionClauseSyntax>();
                if(lambdaDec.Any())
                {
                    var selfReference = lambdaDec.First().DescendantNodes()
                        .OfType<IdentifierNameSyntax>()
                        .Where(id => id.Identifier.ValueText == propertyDeclaration.Identifier.ValueText);
                    if (selfReference.Any())
                    {
                        // Create the diagnostic
                        var primaryLocation = propertyDeclaration.Identifier.GetLocation();
                        var additionalLocations = from sr in selfReference select sr.Identifier.GetLocation();
                        var diagnostic = Diagnostic.Create(
                            PropertySelfReferenceRule,
                            primaryLocation,
                            additionalLocations,
                            new string[] { propertyDeclaration.Identifier.ValueText, "get" }
                            );
                        // Report the diagnostic
                        context.ReportDiagnostic(diagnostic);
                    }
                }
            }
        }

        
    }
}